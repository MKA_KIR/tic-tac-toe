import React, {useState, useEffect} from 'react';

import Board from "../Board";
import Score from "../Score";

import {calculateWinner} from "../../../../helper";

import './Game.scss';

const Game = () => {
    const [board, setBoard] = useState(Array(9).fill(null))
    const [xIsNext, setXIsNext] = useState(true)
    const [result, setResult] = useState({X: 0, O: 0})
    const [gameWinner, setGameWinner] = useState(null)

    useEffect(() => {
        if (gameWinner) {
            setTimeout(() => {
                setBoard(Array(9).fill(null))
                setXIsNext(true)
                setGameWinner(null)
            }, 2000)
        }
    }, [gameWinner])

    const handleClick = (idx) => {
        const boardCopy = [...board]
        if (boardCopy[idx] || gameWinner) return
        boardCopy[idx] = xIsNext ? 'X' : 'O'
        const winner = calculateWinner(boardCopy)
        setBoard(boardCopy)
        const nullIdx = boardCopy.findIndex((item) => item === null)
        if (winner) {
            const newResult = {...result}
            newResult[winner]++
            setResult(newResult)
            setGameWinner(`Player ${winner === 'O' ? '2' : ''}`)
            return
        } else if (nullIdx === -1) {
            setGameWinner(`Draw`)
            return
        }
        setXIsNext(!xIsNext)
    }

    let winnerLine = null;
    if (gameWinner && gameWinner.includes('Player')) {
        const winner = xIsNext ? 'X' : 'O'
        const winnerName = Array(3).fill(winner).join('')
        const splitBoardHorizontal = board.reduce((accumulator, currentValue, index) => {
            const newIndex = Math.floor(index / 3);
            const lastAccumIndex = accumulator.length - 1
            if (newIndex === lastAccumIndex) {
                accumulator[lastAccumIndex] += currentValue
            } else {
                accumulator[newIndex] = currentValue
            }
            return accumulator
        }, [''])
        if (splitBoardHorizontal.includes(winnerName)) {
            const index = splitBoardHorizontal.indexOf(winnerName)
            winnerLine = <div className={`winner-line horizontal horizontal-${index}`}></div>
        }
        const splitBoardVertical = board.reduce((accumulator, currentValue, index) => {
            const newIndex = index % 3;
            accumulator[newIndex] += currentValue
            return accumulator
        }, ['', '', ''])
        if (splitBoardVertical.includes(winnerName)) {
            const index = splitBoardVertical.indexOf(winnerName)
            winnerLine = <div className={`winner-line vertical vertical-${index}`}></div>
        }
            const splitBoardOblique = []
            splitBoardOblique[0] = board[0]+board[4]+board[8]
            splitBoardOblique[1] = board[2]+board[4]+board[6]
        if (splitBoardOblique.includes(winnerName)) {
            const index = splitBoardOblique.indexOf(winnerName)
            winnerLine = <div className={`winner-line oblique oblique-${index}`}></div>
        }
    }
    return (
        <div className="game-wrapper">
            <div className='game-body'>
                <h2 className="game-result">{gameWinner}</h2>
                <Board squares={board} winnerLine={winnerLine} click={handleClick}/>
                <Score result={result}/>
            </div>
        </div>
    );
};

export default Game;
