import React from 'react';

import './Board.scss';

import Square from "../Square";

const Board = ({squares, click, winnerLine}) => {
    const squareElements =squares.map((square, idx) => (
        <Square key={idx} value={square} onClick={() => click(idx)}/>
    ))

    return (
        <div className="game-board">
            {winnerLine}
            {squareElements}
        </div>
    );
};

export default Board;
