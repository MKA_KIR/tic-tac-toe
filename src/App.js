import '../src/shared/styles/main.scss';
import Game from "./clients/tic-tac-toe/components/Game";

function App() {
  return (
    <div className="App">
    <Game />
    </div>
  );
}

export default App;
