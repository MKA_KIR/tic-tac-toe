import React from 'react';

import './Square.scss';

const Square = ({onClick, value}) => {

    return (
        <span className="game-square" onClick={onClick}>{value}</span>
    );
};

export default Square;
