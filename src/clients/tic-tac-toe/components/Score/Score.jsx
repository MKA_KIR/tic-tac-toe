import React from 'react';

import './Score.scss';

const Score = ({result}) => {

    return (
        <div className='game-score'>
            <p className="game-score-text">Score</p>
            <p className="game-score-text">Player: {result.X}</p>
            <p className="game-score-text">Player 2: {result.O}</p>
        </div>
    );
};

export default React.memo(Score);
